var ServicenowEbond = Class.create({
	insertServiceNowIncident: function(record) {
		var client = _getHTTP('insert', record.id);
		var jsonBody = {};
		var status = _map_status($record.status.getDisplayValue(), "DS");
		jsonBody.short_description = $record.title;
		jsonBody.description = $record.description;
		jsonBody.correlation_id = $record.id;
		jsonBody.state = status;
		console.log('DRS ' + JSON.stringify(jsonBody));
		client.setRequestBody(jsonBody);
		var userResponse = client.post();
	},
	updateServiceNowIncident: function(record) {
		console.log('Update');
		var client = _getHTTP('update', record.id);
		var jsonBody = {};
		var status = _map_status($record.status.getDisplayValue(), "DS");
		jsonBody.short_description = $record.title;
		jsonBody.description = $record.description;
		jsonBody.correlation_id = $record.id;
		jsonBody.state = status;
		console.log('DRS ' + JSON.stringify(jsonBody));

		client.setRequestBody(jsonBody);
		var userResponse = client.put();
		
	},
	updateServiceNowWorkNotes: function(record) {
		var jsonBody = {};
		var work_notes = isJson($record.text);
		console.log('DRS Work notes ' + work_notes);
		jsonBody.work_notes = work_notes;
		console.log('DRS JSON BODY' + JSON.stringify(jsonBody));
		client.setRequestBody(jsonBody);
		var userResponse = client.put();
		console.log(
			'ServiceNow Sync Speaks HTTP code: ' +
				userResponse.getResponseCode()
		);
		var responseObject = JSON.parse(userResponse.getResponseBody());
		console.log(
			'ServiceNow Sync Speaks Response' + JSON.stringify(responseObject)
		);
		if (responseObject.error) {
			console.log(
				'ServiceNow sync error ' + responseObject.error.messgage
			);
			//TODO: Insert Error into speaks activity log
		} else if (responseObject.inserted) {
			$record.addSchemaId('9dd667bbb3774dc3a2b8f2e235987639');
			$record.servicenow_id = responseObject.inserted;
			$record.update();
		}

		function isJson(str) {
			try {
				var json = JSON.parse(str);
			} catch (e) {
				return str;
			}
			return json.ops[0].insert;
		}
	},

	_getHTTP: function(op, id) {
		var url;
		var username;
		var password;
		if (op == 'insert') {
			url =
				'https://cernasolutionsllcdemo7.service-now.com/api/cerso/dreamtsoftbond/story';
		} else if (op == 'update') {
			url =
				'https://cernasolutionsllcdemo7.service-now.com/api/cerso/dreamtsoftbond/story/' +
				id;
		}
		var cred = new FRecord('credential');
		cred.addSearch('name', 'ServiceNow');
		cred.search();
		if (cred.next()) {
			cred.setFullRow(true);
			console.log(cred.username);
			console.log(cred.password.getDecryptedValue());
			username = cred.username;
			password = cred.password.getDecryptedValue();
		}
		var HTTPScriptable = require('core/HTTPScriptable');
		var client = new HTTPScriptable(url);
		client.setBasicAuthentication(username, password, true);
		client.setHeader('Content-Type', 'application/json');
		return client;
	},

	_map_status: function(status, source) {
		var stati = [
			{ DS: 'New', SN: 'Draft' },
			{ DS: 'Work in progress', SN: 'Work in progress' },
			{ DS: 'On hold', SN: 'Ready' },
			{ DS: 'Resolved', SN: 'Testing' },
			{ DS: 'Closed', SN: 'Complete' },
			{ DS: 'Cancelled', SN: 'Cancelled' }
		];

		for (var i = 0; i < stati.length; i++) {
			if (stati[i][source] === nameKey) {
				return stati[i];
			}
		}
	},

	className: 'ServicenowEbond'
});

module.exports = ServicenowEbond;
