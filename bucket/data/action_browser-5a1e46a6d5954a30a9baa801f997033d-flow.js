{
  "flow": [
    {
      "data": { },
      "height": 40,
      "id": "start",
      "left": 0,
      "output": { },
      "routes": [
        {
          "condition": "",
          "order": 0,
          "text": "Always",
          "to": "czag9jrz",
          "type": "always"
        }
      ],
      "static_routes": true,
      "stuck": true,
      "text": "Start",
      "top": 0,
      "type": "box",
      "width": 100
    },
    {
      "data": {
        "hide_loading": "0",
        "id": "czag9jrz",
        "isList": false,
        "name": "Run Script",
        "script": "var ServiceowEbond = require('ds.sm/servicenow/ServiceowEbond');\nvar serviceowEbond = new ServicenowEbond();\nvar insertIncident = serviceowEbond.insertServiceNowIncident($record);",
        "show_logging": "0",
        "suppress_messages": "0",
        "use_ids": "0"
      },
      "height": 40,
      "id": "czag9jrz",
      "left": 110,
      "output": { },
      "routes": [
        {
          "condition": "success:eq:1:true",
          "order": 0,
          "text": "Success",
          "type": "success"
        },
        {
          "condition": "success:eq:0:false",
          "order": 1,
          "text": "Failure",
          "type": "error"
        }
      ],
      "text": "Run Script",
      "top": 0,
      "type": "script",
      "width": 100
    }
  ],
  "inputs": [
    {
      "attributes": {
        "buckets": "true",
        "choice_type": "typeahead"
      },
      "labels": {
        "label": "Bucket",
        "labelp": "Buckets"
      },
      "name": "bucketId",
      "type": "choice"
    },
    {
      "attributes": {
        "bucket_slot": "bucketId",
        "choice_type": "typeahead"
      },
      "labels": {
        "label": "Record",
        "labelp": "Records"
      },
      "name": "record",
      "type": "choice"
    }
  ],
  "outputs": [ ],
  "properties": {
    "async": false,
    "flow_type": "f2b9095bbd2441709f25a07cdf240534"
  },
  "variables": [ ]
}