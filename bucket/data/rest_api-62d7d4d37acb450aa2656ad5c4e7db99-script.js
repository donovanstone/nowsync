var serviceNowUpdate = ARestAPI.create({
	process: function() {
		wsconsole.log('/console/mymsgs', 'ServiceNow insert' + this.getBody());
		if (this.getBody()) {
			var jsonBodyStr = this.getBody();
			var jsonBody = JSON.parse(jsonBodyStr);
			var stringResponse = JSON.stringify(jsonBody);
			var msg = stringResponse;
		}
		var ioIn = new FRecord('io_in');
		ioIn.source = "ServiceNow Update";
		ioIn.message = JSON.stringify(this.getBody);
		ioIn.insert();
		this.setContentType('application/json');
		this.setResponseCode(200);
		msg = { message: 'Success' };
		return msg;
	},

	className: 'serviceNowUpdate'
});

module.exports = serviceNowUpdate;
